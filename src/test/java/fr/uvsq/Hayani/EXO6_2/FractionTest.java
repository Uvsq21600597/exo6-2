package fr.uvsq.Hayani.EXO6_2;


import static org.junit.Assert.*;

import org.junit.Test;





public class FractionTest {
	
		
		@Test(expected = IllegalArgumentException.class)
		public void ConstructeurAvecUnDenominateurEgaleZero() {
			
			 Fraction f1= new Fraction(3,0);
			 
			
		}
		
		@Test
		 public void ConstructeurAvecUnDenominateurMoinsDeZero() {
			
			 Fraction f1=new Fraction(13,-10);
			 assertEquals(-13, f1.getNumerateur());
			    
			
		}
		 @Test
		 public void ConstructeurSansParametres() {
			
			 Fraction f1=new Fraction();
			 assertEquals(0, f1.getNumerateur());
			 assertEquals(1,f1.getDenominateur());
			    
			
		}
		 @Test
		 public void ConstanteZero() {
			
			 Fraction f1= Fraction.ZERO;
			 assertEquals(0, f1.getNumerateur());
			 assertEquals(1,f1.getDenominateur());
			    
			
		}
		
		 @Test
		 public void ConstanteUN() {
			
			 Fraction f1= Fraction.UN;
			 assertEquals(1, f1.getNumerateur());
			 assertEquals(1,f1.getDenominateur());
			    
			
		}
		 
		 @Test
		 public void GeFractionTest() {
			
			 Fraction f1=new Fraction(10,4);
			 assertEquals(10, f1.getNumerateur());
			 assertEquals(4,f1.getDenominateur());
		}
		 @Test
		 public void GeFractionvirguleTest() {
				
			 Fraction f1=new Fraction(10,4);
			double result= (double)f1.getNumerateur()/f1.getDenominateur();
			 assertEquals((Double)result,(Double)f1.ConsultationFraction());
			 
		}

		 @Test
		 public void ToStringTestAvecLesdeuxParametres() {
				
			 Fraction f1=new Fraction(10,4);
			String expected ="10 / 4";
			 assertEquals(expected,f1.toString());
			 
		}
		 @Test
		 public void ToStringTestAvecNumerateurEgaleZero() {
				
			 Fraction f1=new Fraction(0,4);
			String expected ="Zero";
			 assertEquals(expected,f1.toString());
			 
		}
		
		 @Test
		 public void ToStringTestAvecDenominateurEgaleUn() {
				
			 Fraction f1=new Fraction(4,1);
			String expected ="4";
			 assertEquals(expected,f1.toString());
			 
		}
		
		 @Test
		 public void EgaliteTest() {
				
			 Fraction f1=new Fraction(1,2);
			 Fraction f2=new Fraction(10,20);
			
			 assertEquals(true,f1.Comparer(f2));
			 
		}
		 
		 @Test
			public void testComparaison(){
				Fraction f1=new Fraction(4,6);
				Fraction f2=new Fraction(2,6);
				String expected =(f2.toString())+" < " +(f1.toString());
				assertEquals(expected,f1.Comparaison(f2));}
			
			
		 
		 
		 
}