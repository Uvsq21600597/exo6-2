package fr.uvsq.Hayani.EXO6_2;

public class Fraction {
	
	final private int numerateur;
	final private int denominateur;

	public static final Fraction UN = new Fraction(1, 1);
	public static final Fraction ZERO = new Fraction(0, 1);
	
	
	public Fraction(int numerateur, int denominateur) {
		
		if (denominateur == 0) {
			throw new IllegalArgumentException("le denominateur ne peut jamais être égal à zéro");
		}
		else {
			if (denominateur < 0) {
		
	           				numerateur = -numerateur;
	           				denominateur=-denominateur;
	   					  }
		
		this.denominateur=denominateur;	
		this.numerateur=numerateur;
		}
		}

		
	public int getNumerateur() {
		return numerateur;
	}


	public int getDenominateur() {
		return denominateur;
	}


	public Fraction(int numerateur) {
			
			this.numerateur=numerateur;
			this.denominateur=1;
			
		}
		
	public Fraction() {
			
			this.numerateur=0;
			this.denominateur=1;
		}

	
	public void Consultation(){
		System.out.println("Numerateur est 	 : "+numerateur);
		System.out.println("Denominateur est : "+denominateur);
	}

	public double ConsultationFraction(){
		return (double)numerateur/denominateur;
		}



	public String toString() {

		final String str;
	    if (this.denominateur == 1) str = Integer.toString(numerateur);
	    else if (this.numerateur == 0) str = "Zero";
	    else str = this.numerateur + " / " + this.denominateur;
	    return str;
	}


	public boolean Comparer(final Fraction fraction) {
		   
		if (this.equals(fraction) || (this.numerateur == fraction.numerateur && this.denominateur == fraction.denominateur) ) return true;
		  
		  int x = (this.numerateur) * fraction.denominateur;
		  int y= ( this.denominateur) * fraction.numerateur;
	  
		  if (x == y)  return true;
		  else  return false;
	    
	}

public String Comparaison(final Fraction f){
		
		String str ="";
		
		if ((this == f) || (numerateur == f.numerateur && denominateur == f.denominateur) ) {
			str= " les deux fractions sont egaux";;
		}
		  
		int a = (numerateur) * f.denominateur;
	    int b = (denominateur) * f.numerateur;
	    
	    if (a == b)  str= " les deux fractions sont egaux";
	    
	    else if (a < b) str= f.toString()+" > "+this.toString();
	    
	    else  str= f.toString()+" < "+this.toString();

		return str;
	}
	
	
	
	
	

}
